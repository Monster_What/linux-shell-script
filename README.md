# Linux Shell Script

Linux shell scripts to auto update the buildtools.jar and generate the new spigot.jar + replace the old spigot.jar from the running server

***Make sure you modify this script to match your current directory configuration!***

<h4><strong>TO USE</strong></h4>
<h3>In order to run this you only need to type <code>./runbuild.sh</code> on a terminal.</h3>
<h4><strong>If it gives an error when trying to run it make sure you run the command</strong><code>$ chmod +x ./runbuild.sh</code> in order to make it executable.</h4>
<h5>Remember <b>$</b> means run this code as a NORMAL USER.</h5>

<h2>This shell is provided and used by [PlayDeca](https://playdeca.com)</h2>
<br>
<a href="https://playdeca.com"><img src="https://playdeca.com/assets/images/logos/PlayDecaBannerPNG.png" style="max-width:100%;" width="600"></a>
<br>
