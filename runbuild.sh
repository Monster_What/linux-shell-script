#!/bin/bash

#Create the "build" directory if it does not exist.
mkdir -p build

#Navigates to the "build" directory.
cd ./build

#Gets the latest BuildTools.jar
wget "https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar" -O BuildTools.jar

#Builds the selected/latest spigot.jar file.
java -jar BuildTools.jar --rev ${1:-latest} --output-dir ./jars

#Deletes all but the latest file on the folder
read -r -p "Do you want to delete all previous versions of spigot? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
cd ./jars
ls -1 spigot-*.jar | sort -r | tail +2 |  xargs -i rm {}

#Rename the file to be Spigot.jar
read -r -p "Do you want to rename the spigot file to spigot.jar? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
mv spigot-*.jar spigot.jar

#Move spigot.jar to the current working directory
read -r -p "Do you want to move the latest spigot file to MAIN? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
cp spigot.jar ../../server/spigot.jar
else
	exit
fi
else
	exit
fi
else
	exit
fi
